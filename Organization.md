청강문화산업대학교 졸작팀 샐러캔스

# Creator
### Product Manager
* 14학번 장우영

### System Manager
* 14학번 유수현

### Event Manager
* 14학번 노민준

# Developer
### Server Programmer
* 16학번 조관희

### Client Programmer
* 14학번 차진민

# Graphics Designer
### Art Director
* 14학번 김지헌

### illustrator
* 16학번 서효진
* 16학번 김애리
* 13학번 최혁준

### Effect
* 16학번 김희경

### 3D Graphics
* 14학번 김성민

# Sound Desinger
* 14학번 유수현