﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworkFist : Firework
{
    public float hitRadius;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        FireworkExecuter executer = obj.GetComponent<FireworkExecuter>();
        executer.hitRadius = hitRadius;
    }
}
