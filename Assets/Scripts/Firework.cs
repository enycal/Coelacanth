﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Firework : ScriptableObject {
    public enum FireworkType
    {
        Range,
        Projectile,
        Installation
    }

    public string fwName;
    public FireworkType fwType;
    public int damage;
    public int capacity;
    public float hitForce;
    public float lifeTime;
    public float coolDown;
    public Sprite uiSprite;
    public AudioClip startSound;

    public virtual void Initialize(GameObject obj)
    {
        FireworkExecuter executer = obj.GetComponent<FireworkExecuter>();

        executer.fwName = fwName;
        executer.fwType = fwType;
        executer.damage = damage;
        executer.capacity = capacity;
        executer.hitForce = hitForce;
        executer.lifetime = lifeTime;
        executer.coolDown = coolDown;

        executer.Initialize();
    }
}
