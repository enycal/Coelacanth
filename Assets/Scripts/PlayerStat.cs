﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : MonoBehaviour {

    public bool onStage = true;     // 스테이지 상에서 살아있음, 번지 X
    public bool alive = true;       // 라이프가 1이상
    public string nickname;         // 닉네임
    public int maxHp = 100;         // 최대 HP
    public int curHp;               // 현재 HP
    public int life = 3;            // 남은 라이프
    public Firework curFirework;

    private PlayerController pc;

    private void Awake()
    {
        curHp = maxHp;
        pc = GetComponent<PlayerController>();
    }

    [PunRPC]
    public void Damage(int dmg)
    {
        if (!onStage) return;

        if (curHp - dmg <= 0)
        {
            curHp = 0;
            pc.Stun();
        }
        else
        {
            curHp -= dmg;
        }
    }

    public void LifeLoss()
    {
        life--;

        if(life <= 0)
        {
            // 라이프 다 없어짐
            alive = false;
            Debug.Log(gameObject.GetPhotonView().owner.NickName + " 사망");
        }
    }
}
