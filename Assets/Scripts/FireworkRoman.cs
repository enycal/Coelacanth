﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Firework/Roman")]
public class FireworkRoman : Firework
{
    public float hitRadius;
    public float projectileSpeed;
    public GameObject projectile_ref;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        FireworkExecuter executer = obj.GetComponent<FireworkExecuter>();
        executer.hitRadius = hitRadius;
        executer.projectileSpeed = projectileSpeed;
        executer.projectile_ref = projectile_ref;

    }
}
