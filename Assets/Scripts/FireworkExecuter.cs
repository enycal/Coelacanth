﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FireworkExecuter : Photon.PunBehaviour {

    public Transform firePoint;
    public string fwName;
    [HideInInspector]
    public Firework.FireworkType fwType;
    [HideInInspector]
    public int damage;
    [HideInInspector]
    public int capacity;
    private int ammo;
    [HideInInspector]
    public float hitForce;
    [HideInInspector]
    public float hitRadius;
    [HideInInspector]
    public float projectileSpeed;
    [HideInInspector]
    public float lifetime;
    [HideInInspector]
    public float coolDown;
    private float elapsedTime;
    [HideInInspector]
    public GameObject projectile_ref;

    private bool fireEnable = true;


    private PlayerController pc;

    private void Awake()
    {
        elapsedTime = 0f;
    }

    private void Start()
    {
        pc = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (elapsedTime >= coolDown)
        {
            fireEnable = true;
        }
        else
        {
            elapsedTime += Time.deltaTime;
        }
    }

    public void Initialize()
    {
        ammo = capacity;
        elapsedTime = 0f;
    }

    public void Execute()
    {
        //gameObject.GetPhotonView().RPC("Fire", PhotonTargets.All, null);
        if(fireEnable)
            FireStart();
    }

    private void FireStart()
    {
        fireEnable = false;
        elapsedTime = 0f;
        pc.SetAnimParam("Attack");

        Fire();
    }

    public void Fire()
    {
        if (!gameObject.GetPhotonView().isMine) return;

        BaseProjectile bullet = PhotonNetwork.Instantiate("Prefabs/Romang_firework_fx", firePoint.position, transform.rotation, 0).GetComponent<BaseProjectile>();

        bullet.damage = damage;
        bullet.hitForce = hitForce;
        bullet.hitRadius = hitRadius;
        bullet.lifetime = lifetime;
        bullet.SetSpeed(projectileSpeed);
    }
}
