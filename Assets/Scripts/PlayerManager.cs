﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PlayerStateHandler
{
	idle,
	run,
	back,
	attack
}

public enum MoveEventState
{
	NONE = 0, // 0000 0000
	LEFT = 1 << 1, // 0000 0010
	RIGHT = 1 << 2, // 0000 0100
	FRONT = 1 << 3, // 0000 1000
	BACK = 1 << 4 // 0001 0000
}

[RequireComponent (typeof (Rigidbody))]
public class PlayerManager : MonoBehaviour
{
	public CharacterController _player;
	public GameObject _obj;
	public Animator _anim;
    
	private int _state;
	public int State
	{
		get { return _state; }
		set { _state = value; }
	}

	public float h, v, rotX, rotY;
	public float _speed, _sensi;
	public float _runSpeed;

	private void Awake()
	{
		_speed = 15.0f;
		_sensi = 5.0f;
	}

	private void Start()
	{
		_player = GetComponent<CharacterController>();
		_anim = GetComponent<Animator>();
		Initialized();
	}

	public virtual void Initialized()
	{
		_runSpeed = 1.25f;
	}
	public virtual void Movement() {}
	void Update()
	{
		KeyState();
		//if (Input.anyKey)
			//Debug.Log(State);
		_speed = Input.GetKey(KeyCode.S) ? 10.0f : 15.0f;
		_runSpeed = Input.GetKey(KeyCode.LeftShift) ? 1.75f : 1.25f;
		_speed = Input.GetKey(KeyCode.LeftShift) ? 30.0f : 15.0f;
		State = (int) MoveEventState.NONE;
		Movement();
	}
	
	void KeyState()
	{
		if (Input.GetKey(KeyCode.W))
		{
			State |= (int) MoveEventState.FRONT;
		} 
		else if (Input.GetKey(KeyCode.A))
		{
			State |= (int) MoveEventState.LEFT;
		} 
		else if (Input.GetKey(KeyCode.S))
		{
			State |= (int) MoveEventState.BACK;
		} 
		else if (Input.GetKey(KeyCode.D))
		{
			State |= (int) MoveEventState.RIGHT;
		} 
	}
}
