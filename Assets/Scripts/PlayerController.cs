﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerAniState
{
    Idle,
    Move,
    Fist,
    Roman,
    Fountain,
    Rocket,
    Butterfly,
    Party,
    KnockBack,
    Stun,
    Fall
}

public enum PlayerState
{
    Idle,
    Attack,
    KnockBack,
    Stun

}

public class PlayerController : Photon.PunBehaviour {

    // Move
    public bool photonMove = true;
    public bool isMine = false;
    public bool isControlable = true;
    public bool isStun = false;
    public float accSpeed = 15.0f;
    public float maxSpeed = 10.0f;
    private Vector3 velocity;
    private Vector2 inputAxis;
    private Vector3 targetDirection;

    private PlayerAniState state = PlayerAniState.Idle;

    // 내부 컴포넌트
    private PlayerStat stat;
    private FireworkExecuter executer;
    private Rigidbody rb;
    private Animator anim;

    // 외부 컴포넌트
    private Transform cameraT;
    private Camera cam;

    // Turn
    private Ray mouseRay;
    private RaycastHit mouseHit;
    
    

    int groundMask;

    // 임시
    public Firework tempFW_ref;


    void Start () {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        stat = GetComponent<PlayerStat>();
        executer = GetComponent<FireworkExecuter>();
        anim = GetComponent<Animator>();
        groundMask = LayerMask.GetMask("Ground");

        stat.curFirework = tempFW_ref;
        stat.curFirework.Initialize(gameObject);

        GameManagerPhoton._instance.cameraController.FindPlayers();
    }

    private void OnDestroy()
    {
        GameManagerPhoton._instance.cameraController.FindPlayers();
    }

    private void Update()
    {
        if (!stat.onStage) return;

        if (!photonMove || photonView.isMine)
        {
            if (isControlable && !isStun)
            {
                if (Input.GetMouseButtonDown(0) && stat.curFirework != null)
                {
                    executer.Execute();
                }
            }

            CheckFall();
            ApplyAnimatorParams();
        }
        
    }

    void FixedUpdate () {
        if (!stat.onStage) return;

        if (!photonMove || photonView.isMine)
        {
            if (isControlable && !isStun)
            {
                velocity = rb.velocity;

                GetInput();
                AddVelocity();
                TurnToMouse();
                ApplyAnimatorParams();

                rb.velocity = velocity;
            }
        }
        
	}

    void GetInput()
    {
        inputAxis.x = Input.GetAxisRaw("Horizontal");
        inputAxis.y = Input.GetAxisRaw("Vertical");

        //targetDirection = (inputAxis.y * Vector3.Scale(cam.transform.forward, new Vector3(1, 0, 1)).normalized + inputAxis.x * cam.transform.right).normalized;
        targetDirection = (inputAxis.y * Vector3.forward + inputAxis.x * Vector3.right).normalized;
    }

    void ApplyAnimatorParams()
    {
        if(inputAxis.magnitude > 0)
        {
            state = PlayerAniState.Move;
        }
        else
        {
            state = PlayerAniState.Idle;
        }


        anim.SetInteger("AniNum", (int)state);
        anim.SetFloat("InputX", inputAxis.x);
        anim.SetFloat("InputY", inputAxis.y);
    }

    public void SetAnimParam(string param)
    {
        anim.SetTrigger(param);
    }

    void Move(Vector3 targetPosition)
    {
        rb.MovePosition(targetPosition);
    }

    void AddVelocity()
    {
        // 입력값이 있다
        if (inputAxis.sqrMagnitude > 0)
        {
            Vector3 addVelocity = targetDirection * accSpeed * Time.fixedDeltaTime;
            float resultMag = (velocity + addVelocity).magnitude;

            if (resultMag < maxSpeed || resultMag < velocity.magnitude)
            {
                velocity += addVelocity;
            }
        }
    }

    void TurnToMouse()
    {
        mouseRay = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(mouseRay, out mouseHit, 50.0f, groundMask))
        {
            Vector3 toDir = Vector3.Scale( mouseHit.point - transform.position, new Vector3(1,0,1)).normalized;

            Quaternion targetRoation = Quaternion.LookRotation(toDir);
            transform.rotation = targetRoation;
        }
    }

    [PunRPC]
    public void Pushed(Vector3 force)
    {
        rb.AddForce(force, ForceMode.Impulse);
    }

    void CheckFall()
    {
        if(transform.position.y < 5)
        {
            photonView.RPC("Fall", PhotonTargets.All);
        }
    }

    [PunRPC]
    public void Fall()
    {
        // 떨어졌을 때
        stat.onStage = false;
        //stat.LifeLoss();
        Invoke("Respawn", 4.0f);
    }

    public void Stun()
    {
        // 기절했을 때
    }

    // 리스폰
    public void Respawn()
    {
        stat.onStage = true;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        GameManagerPhoton._instance.RespawnPlayer(transform);
    }
}
