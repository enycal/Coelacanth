﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerPhoton : Photon.PunBehaviour
{
    public static GameManagerPhoton _instance;

    public Transform[] playerGenPos = null;

    public CameraController cameraController;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {
        cameraController = Camera.main.transform.parent.GetComponent<CameraController>();

        PhotonNetwork.isMessageQueueRunning = true;
        CreatePlayer();
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        Debug.Log("플레이어 접속:" + player.NickName);
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        cameraController.FindPlayers();
        Debug.Log("플레이어 퇴장:" + otherPlayer.NickName);
    }

    // 플레이어 생성
    public void CreatePlayer()
    {
        // PhotonNetwork.room : 접속한 방 정보
        Room room = PhotonNetwork.room;
        Vector3 pos = playerGenPos[room.PlayerCount - 1].position;

        GameObject selfPlayer = PhotonNetwork.Instantiate("Prefabs/PlayerDummy_0415", pos, Quaternion.identity, 0);

        Debug.Log("플레이어 생성");
    }

    public void RespawnPlayer(Transform playerTF)
    {
        Room room = PhotonNetwork.room;
        Vector3 pos = playerGenPos[room.PlayerCount - 1].position;
        pos.y += 0.5f;

        playerTF.position = pos;
    }
    
    

}
