﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Photon.PunBehaviour {
    
    public float smoothMoveTime = 0.3f;
    public float zoomAddOffset = 6.0f;
    [Range(0.5f, 2)]
    public float zoomXFactor = 0.85f;
    [Range(0.5f, 2)]
    public float zoomZFactor = 1.0f;

    // Pivot
    private Vector3 positionVelocity;
    private Vector3 centerPosition = Vector3.zero;
    private Vector3 minPos = Vector3.zero;
    private Vector3 maxPos = Vector3.zero;

    // 실제 카메라
    private Vector3 offsetDirection;
    private Vector3 offsetPosition;
    private Vector3 offsetVelocity;

    private List<GameObject> playerList = new List<GameObject>();
    private Transform camT;

    [HideInInspector]
    public PhotonView cameraPhotonView;

    private void Awake()
    {
        cameraPhotonView = GetComponent<PhotonView>();
    }

    void Start () {
        camT = Camera.main.transform;
        offsetDirection = (camT.position - transform.position).normalized;
        centerPosition.y = transform.position.y;
    }
	
	void FixedUpdate () {

        if (playerList.Count <= 0) return;

        CalculateMinMax();
        CalculateCenter2();
        AutoZoom();
        SmoothMovement();
    }

    public void FindPlayers()
    {
        playerList.Clear();
        playerList.AddRange(GameObject.FindGameObjectsWithTag("Player"));
    }

    // 최소, 최대 위치 계산
    void CalculateMinMax()
    {
        minPos = playerList[0].transform.position;
        maxPos = playerList[0].transform.position;

        for (int i = 1; i < playerList.Count; i++)
        {
            if (!playerList[i].gameObject.activeSelf) continue;
            if (!playerList[i].GetComponent<PlayerStat>().onStage) continue;

            if (minPos.x > playerList[i].transform.position.x)
                minPos.x = playerList[i].transform.position.x;
            if (minPos.z > playerList[i].transform.position.z)
                minPos.z = playerList[i].transform.position.z;

            if (maxPos.x < playerList[i].transform.position.x)
                maxPos.x = playerList[i].transform.position.x;
            if (maxPos.z < playerList[i].transform.position.z)
                maxPos.z = playerList[i].transform.position.z;
        }
    }

    // 전체적인 위치만 판단
    void CalculateCenter()
    {
        centerPosition.x = (minPos.x + maxPos.x) / 2;
        centerPosition.z = (minPos.z + maxPos.z) / 2;
    }

    // 사람 몰려있는 곳에 좀 더 집중 됨
    void CalculateCenter2()
    {
        Vector3 sumPos = Vector3.zero;
        int count = 0;

        for(int i=0; i<playerList.Count; i++)
        {
            if (!playerList[i].gameObject.activeSelf) continue;
            if (!playerList[i].GetComponent<PlayerStat>().onStage) continue;

            sumPos += playerList[i].transform.position;
            count++;
        }

        if(count > 0)
            centerPosition = sumPos / count;
        else
        {
            centerPosition = new Vector3(7f, 7f, 0f);
        }
    }

    // 부드러운 움직임
    void SmoothMovement()
    {
        // Pivot 이동
        transform.position = Vector3.SmoothDamp(transform.position, centerPosition, ref positionVelocity, smoothMoveTime);

        // 카메라 이동(줌 인,아웃)
        camT.localPosition = Vector3.SmoothDamp(camT.localPosition, offsetPosition, ref offsetVelocity, smoothMoveTime);
    }

    // 줌 길이 계산
    void AutoZoom()
    {
        float subX = maxPos.x - minPos.x;
        float subZ = maxPos.z - minPos.z;
        float bigger;

        if (subX > subZ)
        {
            bigger = subX * zoomXFactor;
        }
        else
        {
            bigger = subZ * zoomZFactor;
        }

        offsetPosition = offsetDirection * (bigger + zoomAddOffset);
    }
}
