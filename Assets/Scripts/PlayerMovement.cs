﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PlayerManager
{
	public override void Initialized()
	{
		base.Initialized();
	}

	public override void Movement()
	{
		base.Movement();
		h = Input.GetAxisRaw("Horizontal");
		v = Input.GetAxisRaw("Vertical");
		rotX = Input.GetAxis("Mouse X") * _sensi;
		rotY = Input.GetAxis("Mouse Y") * _sensi;
		Vector3 _movement = new Vector3(h, 0.0f, v);
		transform.Rotate(0, rotX, 0);
		//_obj.transform.Rotate(-rotY, 0, 0);
		_movement = transform.rotation * _movement;
		_player.Move((_movement * _speed) * Time.deltaTime);
	}
}